��    /      �                        !     5     P      Y     z  <        �     �     �     �                          +     =     B     H  	   O     Y  	   e     o     {  "   �  %   �      �     �     �     �               2     A     U     c     k  A   r     �     �     �     �     �  #   �     !     ?  �  _     �               3     A     a  D   h  #   �  $   �     �     	     	     	     $	     +	     J	     `	     g	     p	     v	     �	     �	     �	     �	  .   �	  0   
  -   2
     `
     g
  
   s
     ~
     �
     �
     �
     �
     �
     �
  D   �
     0     @  .   H     w     �  #   �     �     �   %(count)d followers %(count)d following %(username)s said %(when)s About me An unexpected error has occurred Back Check your email for the instructions to reset your password Click to Register! Click to Reset It Edit Profile Edit your profile Email Explore Follow Forgot Your Password? Hi, %(username)s! Home Login Logout New User? Newer posts Not Found Older posts Password Please log in to access this page. Please use a different email address. Please use a different username. Profile Register Remember Me Repeat Password Request Password Reset Reset Password Reset Your Password Say something Sign In Submit The administrator has been notified. Sorry for the inconvenience! Unfollow User User %(username)s not found. Username Welcome to Microblog You are not following %(username)s. Your changes have been saved. [Microblog] Reset Your Password Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2019-09-20 14:57+0530
PO-Revision-Date: 2017-09-29 23:25-0700
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: es
Language-Team: es <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.7.0
 %(count)d seguidores siguiendo a %(count)d %(username)s dijo %(when)s Acerca de mí Ha ocurrido un error inesperado Atrás Busca en tu email las instrucciones para crear una nueva contraseña ¡Haz click aquí para registrarte! Haz click aquí para pedir una nueva Editar Perfil Editar tu perfil Email Explorar Seguir ¿Te olvidaste tu contraseña? ¡Hola, %(username)s! Inicio Ingresar Salir ¿Usuario Nuevo? Artículos siguientes Página No Encontrada Artículos previos Contraseña Por favor ingrese para acceder a esta página. Por favor use una dirección de email diferente. Por favor use un nombre de usuario diferente. Perfil Registrarse Recordarme Repetir Contraseña Pedir una nueva contraseña Nueva Contraseña Nueva Contraseña Dí algo Ingresar Enviar El administrador ha sido notificado. ¡Lamentamos la inconveniencia! Dejar de seguir Usuario El usuario %(username)s no ha sido encontrado. Nombre de usuario Bienvenido a Microblog No estás siguiendo a %(username)s. Tus cambios han sido salvados. [Microblog] Nueva Contraseña 