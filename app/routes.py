from flask import render_template, flash, redirect, url_for, request, g
from app import module,db
from app.forms import LoginForm, RegistrationForm, EditProfileForm, PostForm, ResetPasswordRequestForm, ResetPasswordForm
from flask_login import current_user, login_user, logout_user, login_required
from app.models import User, Post
from werkzeug.urls import url_parse
from datetime import datetime
from app.email import send_password_reset_email
from flask_babel import _, get_locale
from guess_language import guess_language
from flask import jsonify
from app.translate import translate

#home page
@module.route('/',methods = ['GET','POST'])
@module.route('/index', methods = ['GET','POST'])
@login_required
def index():
    form = PostForm()
    if form.validate_on_submit():
        language = guess_language(form.post.data)
        if language == 'UNKNOWN' or len(language)>5:
            language = ''
        post = Post(body = form.post.data, author=current_user,language= language)
        db.session.add(post)
        db.session.commit()
        flash(_('Your post is sucessful!'))
        return redirect(url_for('index'))
    page = request.args.get('page',1,type=int)
    posts = current_user.followed_posts().paginate(page, module.config['POST_PER_PAGE'], False)
    next_url = url_for('index',page=posts.next_num) if posts.has_next else None
    prev_url = url_for('index',page=posts.prev_num) if posts.has_prev else None
    return render_template('index.html', title=_('Home Page'), posts=posts.items, form = form,next_url = next_url, prev_url = prev_url)

#login page
@module.route('/login',methods = ['GET','POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username = form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash(_('Invalid credentials'))
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
        # return redirect(url_for('/index'))
    return render_template('login.html', title=_('Sign In'), form=form)

#to logout
@module.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

#to register a new user
@module.route('/register', methods = ['GET','POST'])
def register():
    #user authenticated hoga to direct index dikha do meaning user shi h
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    #agar form validate hogya h to neeche ka function execute kro
    if form.validate_on_submit():
        #user is sending the data to the database kuinki humara schema hi aisa h!!
        user = User(username = form.username.data, email= form.email.data)
        #password hum set karenge is function se kuin ki humne model mein ek function banaya h to set password
        user.set_password(form.password.data)
        #yahan hum apna data database mein save karaenge
        db.session.add(user)
        db.session.commit()
        #flash message store hoga to display our sucessful registration
        flash(_('Congrats, You registered sucessfully!'))
        return redirect(url_for('login'))
    return render_template('register.html', title=_('Register'), form=form)


#user creation
@module.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    page = request.args.get('page', 1, type=int)
    posts = user.posts.order_by(Post.timestamp.desc()).paginate(
        page, module.config['POST_PER_PAGE'], False)
    next_url = url_for('user', username=user.username, page=posts.next_num) \
        if posts.has_next else None
    prev_url = url_for('user', username=user.username, page=posts.prev_num) \
        if posts.has_prev else None
    return render_template("user.html",user=user,posts=posts.items, next_url = next_url, prev_url = prev_url)

@module.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
    g.locale = str(get_locale())

#form to edit profile
@module.route('/edit_profile', methods = ['GET','POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash(_('Your changes have been saved.'))
        return redirect(url_for('edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', title=_('Edit Profile'),
                           form=form)

#follow and unfollow setup
@module.route('/follow/<username>')
@login_required
def follow(username):
    user  = User.query.filter_by(username=username).first()
    if user is None:
        flash(_('User %(username)s not found.', username=username))
        return redirect(url_for('index'))
    if user == current_user:
        flash(_('You cannot follow yourself'))
        return redirect(url_for('user',username=username))
    current_user.follow(user)
    db.session.commit()
    flash(_('You are not following %(username)s.', username=username))
    return redirect(url_for('user',username=username))


@module.route('/unfollow/<username>')
@login_required
def unfollow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('User {} not found.'.format(username))
        return redirect(url_for('index'))
    if user == current_user:
        flash('You cannot unfollow yourself!')
        return redirect(url_for('user', username=username))
    current_user.unfollow(user)
    db.session.commit()
    flash('You are not following {}.'.format(username))
    return redirect(url_for('user', username=username))

#explore other global users
@module.route('/explore')
@login_required
def explore():

    page = request.args.get('page',1, type=int)
    posts = Post.query.order_by(Post.timestamp.desc()).paginate(page, module.config['POST_PER_PAGE'],False)
    next_url = url_for('index',page=posts.next_num) if posts.has_next else None
    prev_url = url_for('index',page=posts.prev_num) if posts.has_prev else None
    return render_template('index.html',title = _('Explore'),posts=posts.items,next_url=next_url, prev_url=prev_url)

#reset your password
@module.route('/reset_password_request', methods=['GET','POST'])
def reset_password_request():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = ResetPasswordRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            send_password_reset_email(user)
        flash(_('Check your email for the instructions to reset your password'))
        return redirect(url_for('login'))
    return render_template('reset_password_request.html',
                           title=_('Reset Password'), form=form)

@module.route('/reset_password/<token>', methods = ['GET','POST'])
def reset_password(token):
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    user = User.verify_reset_password_token(token)
    if not user:
        return redirect(url_for('index'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        db.session.commit()
        flash(_('Your password has been reset'))
        return redirect(url_for('login'))
    return render_template('reset_password.html', form=form)


#to translate to another language
@module.route('/translate', methods =['POST'])
@login_required
def translate_text():
    return jsonify({'text': translate(request.form['text'],
                                      request.form['source_language'],
                                      request.form['dest_language'])})