import logging
import os
from flask import Flask, request
from config import Config
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_moment import Moment
from flask_babel import Babel, lazy_gettext as _l
from flask_mail import Mail
from logging.handlers import SMTPHandler, RotatingFileHandler

module = Flask(__name__)
module.config.from_object(Config)
db = SQLAlchemy(module)
mail = Mail(module)
bootstrap = Bootstrap(module)
moment = Moment(module)
babel = Babel(module)
migrate = Migrate(module, db)
login = LoginManager(module)
login.login_view = 'login'
login.login_message = _l('Please log in to access this page.')




if not module.debug:
    if module.config['MAIL_SERVER']:
        auth = None
        if module.config['MAIL_USERNAME'] or module.config['MAIL_PASSWORD']:
            auth = (module.config['MAIL_USERNAME'], module.config['MAIL_PASSWORD'])
        secure = None
        if module.config['MAIL_USE_TLS']:
            secure = ()
        mail_handler = SMTPHandler(
            mailhost=(module.config['MAIL_SERVER'], module.config['MAIL_PORT']),
            fromaddr='no-reply@' + module.config['MAIL_SERVER'],
            toaddrs=module.config['ADMINS'], subject='Blog Failure',
            credentials=auth, secure=secure)
        mail_handler.setLevel(logging.ERROR)
        module.logger.addHandler(mail_handler)


if not module.debug:
    # logging errors

    if not os.path.exists('logs'):
        os.mkdir('logs')
    file_handler = RotatingFileHandler('logs/blog.log', maxBytes=10240,
                                       backupCount=10)
    file_handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
    file_handler.setLevel(logging.INFO)
    module.logger.addHandler(file_handler)

    module.logger.setLevel(logging.INFO)
    module.logger.info('blog error info')

@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(module.config['LANGUAGES'])


from app import routes, models, errors
